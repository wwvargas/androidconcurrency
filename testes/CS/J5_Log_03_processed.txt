====================================================================================================
========================================== CONCURRENT SUM ==========================================
====================================================================================================
Repetition 1: Report execution of Threads implementation, adding 262144 numbers and using 1 tasks
Time: 269 ms
Repetition 2: Report execution of Threads implementation, adding 262144 numbers and using 1 tasks
Time: 279 ms
Repetition 3: Report execution of Threads implementation, adding 262144 numbers and using 1 tasks
Time: 289 ms
Repetition 4: Report execution of Threads implementation, adding 262144 numbers and using 1 tasks
Time: 266 ms
Repetition 5: Report execution of Threads implementation, adding 262144 numbers and using 1 tasks
Time: 265 ms
Repetition 6: Report execution of Threads implementation, adding 262144 numbers and using 1 tasks
Time: 296 ms
Repetition 7: Report execution of Threads implementation, adding 262144 numbers and using 1 tasks
Time: 470 ms
Repetition 8: Report execution of Threads implementation, adding 262144 numbers and using 1 tasks
Time: 297 ms
Repetition 9: Report execution of Threads implementation, adding 262144 numbers and using 1 tasks
Time: 363 ms
Repetition 10: Report execution of Threads implementation, adding 262144 numbers and using 1 tasks
Time: 454 ms
Repetition 1: Report execution of Threads implementation, adding 262144 numbers and using 16 tasks
Time: 279 ms
Repetition 2: Report execution of Threads implementation, adding 262144 numbers and using 16 tasks
Time: 278 ms
Repetition 3: Report execution of Threads implementation, adding 262144 numbers and using 16 tasks
Time: 259 ms
Repetition 4: Report execution of Threads implementation, adding 262144 numbers and using 16 tasks
Time: 250 ms
Repetition 5: Report execution of Threads implementation, adding 262144 numbers and using 16 tasks
Time: 256 ms
Repetition 6: Report execution of Threads implementation, adding 262144 numbers and using 16 tasks
Time: 246 ms
Repetition 7: Report execution of Threads implementation, adding 262144 numbers and using 16 tasks
Time: 252 ms
Repetition 8: Report execution of Threads implementation, adding 262144 numbers and using 16 tasks
Time: 242 ms
Repetition 9: Report execution of Threads implementation, adding 262144 numbers and using 16 tasks
Time: 243 ms
Repetition 10: Report execution of Threads implementation, adding 262144 numbers and using 16 tasks
Time: 268 ms
Repetition 1: Report execution of Threads implementation, adding 262144 numbers and using 256 tasks
Time: 803 ms
Repetition 2: Report execution of Threads implementation, adding 262144 numbers and using 256 tasks
Time: 785 ms
Repetition 3: Report execution of Threads implementation, adding 262144 numbers and using 256 tasks
Time: 795 ms
Repetition 4: Report execution of Threads implementation, adding 262144 numbers and using 256 tasks
Time: 763 ms
Repetition 5: Report execution of Threads implementation, adding 262144 numbers and using 256 tasks
Time: 817 ms
Repetition 6: Report execution of Threads implementation, adding 262144 numbers and using 256 tasks
Time: 760 ms
Repetition 7: Report execution of Threads implementation, adding 262144 numbers and using 256 tasks
Time: 781 ms
Repetition 8: Report execution of Threads implementation, adding 262144 numbers and using 256 tasks
Time: 755 ms
Repetition 9: Report execution of Threads implementation, adding 262144 numbers and using 256 tasks
Time: 751 ms
Repetition 10: Report execution of Threads implementation, adding 262144 numbers and using 256 tasks
Time: 746 ms
Repetition 1: Report execution of Threads implementation, adding 1048576 numbers and using 1 tasks
Time: 1039 ms
Repetition 2: Report execution of Threads implementation, adding 1048576 numbers and using 1 tasks
Time: 1044 ms
Repetition 3: Report execution of Threads implementation, adding 1048576 numbers and using 1 tasks
Time: 1036 ms
Repetition 4: Report execution of Threads implementation, adding 1048576 numbers and using 1 tasks
Time: 1089 ms
Repetition 5: Report execution of Threads implementation, adding 1048576 numbers and using 1 tasks
Time: 1095 ms
Repetition 6: Report execution of Threads implementation, adding 1048576 numbers and using 1 tasks
Time: 1041 ms
Repetition 7: Report execution of Threads implementation, adding 1048576 numbers and using 1 tasks
Time: 1051 ms
Repetition 8: Report execution of Threads implementation, adding 1048576 numbers and using 1 tasks
Time: 1062 ms
Repetition 9: Report execution of Threads implementation, adding 1048576 numbers and using 1 tasks
Time: 1035 ms
Repetition 10: Report execution of Threads implementation, adding 1048576 numbers and using 1 tasks
Time: 1352 ms
Repetition 1: Report execution of Threads implementation, adding 1048576 numbers and using 16 tasks
Time: 809 ms
Repetition 2: Report execution of Threads implementation, adding 1048576 numbers and using 16 tasks
Time: 871 ms
Repetition 3: Report execution of Threads implementation, adding 1048576 numbers and using 16 tasks
Time: 825 ms
Repetition 4: Report execution of Threads implementation, adding 1048576 numbers and using 16 tasks
Time: 803 ms
Repetition 5: Report execution of Threads implementation, adding 1048576 numbers and using 16 tasks
Time: 825 ms
Repetition 6: Report execution of Threads implementation, adding 1048576 numbers and using 16 tasks
Time: 817 ms
Repetition 7: Report execution of Threads implementation, adding 1048576 numbers and using 16 tasks
Time: 850 ms
Repetition 8: Report execution of Threads implementation, adding 1048576 numbers and using 16 tasks
Time: 827 ms
Repetition 9: Report execution of Threads implementation, adding 1048576 numbers and using 16 tasks
Time: 837 ms
Repetition 10: Report execution of Threads implementation, adding 1048576 numbers and using 16 tasks
Time: 815 ms
Repetition 1: Report execution of Threads implementation, adding 1048576 numbers and using 256 tasks
Time: 1977 ms
Repetition 2: Report execution of Threads implementation, adding 1048576 numbers and using 256 tasks
Time: 1729 ms
Repetition 3: Report execution of Threads implementation, adding 1048576 numbers and using 256 tasks
Time: 1714 ms
Repetition 4: Report execution of Threads implementation, adding 1048576 numbers and using 256 tasks
Time: 1730 ms
Repetition 5: Report execution of Threads implementation, adding 1048576 numbers and using 256 tasks
Time: 1786 ms
Repetition 6: Report execution of Threads implementation, adding 1048576 numbers and using 256 tasks
Time: 1830 ms
Repetition 7: Report execution of Threads implementation, adding 1048576 numbers and using 256 tasks
Time: 1436 ms
Repetition 8: Report execution of Threads implementation, adding 1048576 numbers and using 256 tasks
Time: 1505 ms
Repetition 9: Report execution of Threads implementation, adding 1048576 numbers and using 256 tasks
Time: 1496 ms
Repetition 10: Report execution of Threads implementation, adding 1048576 numbers and using 256 tasks
Time: 1465 ms
Repetition 1: Report execution of ThreadPool implementation, adding 262144 numbers and using 1 tasks
Time: 284 ms
Repetition 2: Report execution of ThreadPool implementation, adding 262144 numbers and using 1 tasks
Time: 271 ms
Repetition 3: Report execution of ThreadPool implementation, adding 262144 numbers and using 1 tasks
Time: 267 ms
Repetition 4: Report execution of ThreadPool implementation, adding 262144 numbers and using 1 tasks
Time: 271 ms
Repetition 5: Report execution of ThreadPool implementation, adding 262144 numbers and using 1 tasks
Time: 273 ms
Repetition 6: Report execution of ThreadPool implementation, adding 262144 numbers and using 1 tasks
Time: 270 ms
Repetition 7: Report execution of ThreadPool implementation, adding 262144 numbers and using 1 tasks
Time: 318 ms
Repetition 8: Report execution of ThreadPool implementation, adding 262144 numbers and using 1 tasks
Time: 265 ms
Repetition 9: Report execution of ThreadPool implementation, adding 262144 numbers and using 1 tasks
Time: 287 ms
Repetition 10: Report execution of ThreadPool implementation, adding 262144 numbers and using 1 tasks
Time: 263 ms
Repetition 1: Report execution of ThreadPool implementation, adding 262144 numbers and using 16 tasks
Time: 201 ms
Repetition 2: Report execution of ThreadPool implementation, adding 262144 numbers and using 16 tasks
Time: 195 ms
Repetition 3: Report execution of ThreadPool implementation, adding 262144 numbers and using 16 tasks
Time: 195 ms
Repetition 4: Report execution of ThreadPool implementation, adding 262144 numbers and using 16 tasks
Time: 199 ms
Repetition 5: Report execution of ThreadPool implementation, adding 262144 numbers and using 16 tasks
Time: 195 ms
Repetition 6: Report execution of ThreadPool implementation, adding 262144 numbers and using 16 tasks
Time: 212 ms
Repetition 7: Report execution of ThreadPool implementation, adding 262144 numbers and using 16 tasks
Time: 210 ms
Repetition 8: Report execution of ThreadPool implementation, adding 262144 numbers and using 16 tasks
Time: 195 ms
Repetition 9: Report execution of ThreadPool implementation, adding 262144 numbers and using 16 tasks
Time: 210 ms
Repetition 10: Report execution of ThreadPool implementation, adding 262144 numbers and using 16 tasks
Time: 193 ms
Repetition 1: Report execution of ThreadPool implementation, adding 262144 numbers and using 256 tasks
Time: 204 ms
Repetition 2: Report execution of ThreadPool implementation, adding 262144 numbers and using 256 tasks
Time: 196 ms
Repetition 3: Report execution of ThreadPool implementation, adding 262144 numbers and using 256 tasks
Time: 199 ms
Repetition 4: Report execution of ThreadPool implementation, adding 262144 numbers and using 256 tasks
Time: 203 ms
Repetition 5: Report execution of ThreadPool implementation, adding 262144 numbers and using 256 tasks
Time: 198 ms
Repetition 6: Report execution of ThreadPool implementation, adding 262144 numbers and using 256 tasks
Time: 209 ms
Repetition 7: Report execution of ThreadPool implementation, adding 262144 numbers and using 256 tasks
Time: 197 ms
Repetition 8: Report execution of ThreadPool implementation, adding 262144 numbers and using 256 tasks
Time: 212 ms
Repetition 9: Report execution of ThreadPool implementation, adding 262144 numbers and using 256 tasks
Time: 200 ms
Repetition 10: Report execution of ThreadPool implementation, adding 262144 numbers and using 256 tasks
Time: 211 ms
Repetition 1: Report execution of ThreadPool implementation, adding 1048576 numbers and using 1 tasks
Time: 1091 ms
Repetition 2: Report execution of ThreadPool implementation, adding 1048576 numbers and using 1 tasks
Time: 1088 ms
Repetition 3: Report execution of ThreadPool implementation, adding 1048576 numbers and using 1 tasks
Time: 1137 ms
Repetition 4: Report execution of ThreadPool implementation, adding 1048576 numbers and using 1 tasks
Time: 1098 ms
Repetition 5: Report execution of ThreadPool implementation, adding 1048576 numbers and using 1 tasks
Time: 1111 ms
Repetition 6: Report execution of ThreadPool implementation, adding 1048576 numbers and using 1 tasks
Time: 1111 ms
Repetition 7: Report execution of ThreadPool implementation, adding 1048576 numbers and using 1 tasks
Time: 1186 ms
Repetition 8: Report execution of ThreadPool implementation, adding 1048576 numbers and using 1 tasks
Time: 1127 ms
Repetition 9: Report execution of ThreadPool implementation, adding 1048576 numbers and using 1 tasks
Time: 1082 ms
Repetition 10: Report execution of ThreadPool implementation, adding 1048576 numbers and using 1 tasks
Time: 1083 ms
Repetition 1: Report execution of ThreadPool implementation, adding 1048576 numbers and using 16 tasks
Time: 794 ms
Repetition 2: Report execution of ThreadPool implementation, adding 1048576 numbers and using 16 tasks
Time: 796 ms
Repetition 3: Report execution of ThreadPool implementation, adding 1048576 numbers and using 16 tasks
Time: 792 ms
Repetition 4: Report execution of ThreadPool implementation, adding 1048576 numbers and using 16 tasks
Time: 810 ms
Repetition 5: Report execution of ThreadPool implementation, adding 1048576 numbers and using 16 tasks
Time: 797 ms
Repetition 6: Report execution of ThreadPool implementation, adding 1048576 numbers and using 16 tasks
Time: 793 ms
Repetition 7: Report execution of ThreadPool implementation, adding 1048576 numbers and using 16 tasks
Time: 816 ms
Repetition 8: Report execution of ThreadPool implementation, adding 1048576 numbers and using 16 tasks
Time: 800 ms
Repetition 9: Report execution of ThreadPool implementation, adding 1048576 numbers and using 16 tasks
Time: 786 ms
Repetition 10: Report execution of ThreadPool implementation, adding 1048576 numbers and using 16 tasks
Time: 795 ms
Repetition 1: Report execution of ThreadPool implementation, adding 1048576 numbers and using 256 tasks
Time: 795 ms
Repetition 2: Report execution of ThreadPool implementation, adding 1048576 numbers and using 256 tasks
Time: 822 ms
Repetition 3: Report execution of ThreadPool implementation, adding 1048576 numbers and using 256 tasks
Time: 803 ms
Repetition 4: Report execution of ThreadPool implementation, adding 1048576 numbers and using 256 tasks
Time: 798 ms
Repetition 5: Report execution of ThreadPool implementation, adding 1048576 numbers and using 256 tasks
Time: 802 ms
Repetition 6: Report execution of ThreadPool implementation, adding 1048576 numbers and using 256 tasks
Time: 782 ms
Repetition 7: Report execution of ThreadPool implementation, adding 1048576 numbers and using 256 tasks
Time: 859 ms
Repetition 8: Report execution of ThreadPool implementation, adding 1048576 numbers and using 256 tasks
Time: 832 ms
Repetition 9: Report execution of ThreadPool implementation, adding 1048576 numbers and using 256 tasks
Time: 832 ms
Repetition 10: Report execution of ThreadPool implementation, adding 1048576 numbers and using 256 tasks
Time: 803 ms
Repetition 1: Report execution of HaMeR framework implementation, adding 262144 numbers and using 1 tasks
Time: 451 ms
Repetition 2: Report execution of HaMeR framework implementation, adding 262144 numbers and using 1 tasks
Time: 398 ms
Repetition 3: Report execution of HaMeR framework implementation, adding 262144 numbers and using 1 tasks
Time: 422 ms
Repetition 4: Report execution of HaMeR framework implementation, adding 262144 numbers and using 1 tasks
Time: 437 ms
Repetition 5: Report execution of HaMeR framework implementation, adding 262144 numbers and using 1 tasks
Time: 345 ms
Repetition 6: Report execution of HaMeR framework implementation, adding 262144 numbers and using 1 tasks
Time: 384 ms
Repetition 7: Report execution of HaMeR framework implementation, adding 262144 numbers and using 1 tasks
Time: 361 ms
Repetition 8: Report execution of HaMeR framework implementation, adding 262144 numbers and using 1 tasks
Time: 450 ms
Repetition 9: Report execution of HaMeR framework implementation, adding 262144 numbers and using 1 tasks
Time: 462 ms
Repetition 10: Report execution of HaMeR framework implementation, adding 262144 numbers and using 1 tasks
Time: 343 ms
Repetition 1: Report execution of HaMeR framework implementation, adding 262144 numbers and using 16 tasks
Time: 350 ms
Repetition 2: Report execution of HaMeR framework implementation, adding 262144 numbers and using 16 tasks
Time: 281 ms
Repetition 3: Report execution of HaMeR framework implementation, adding 262144 numbers and using 16 tasks
Time: 285 ms
Repetition 4: Report execution of HaMeR framework implementation, adding 262144 numbers and using 16 tasks
Time: 289 ms
Repetition 5: Report execution of HaMeR framework implementation, adding 262144 numbers and using 16 tasks
Time: 291 ms
Repetition 6: Report execution of HaMeR framework implementation, adding 262144 numbers and using 16 tasks
Time: 265 ms
Repetition 7: Report execution of HaMeR framework implementation, adding 262144 numbers and using 16 tasks
Time: 271 ms
Repetition 8: Report execution of HaMeR framework implementation, adding 262144 numbers and using 16 tasks
Time: 282 ms
Repetition 9: Report execution of HaMeR framework implementation, adding 262144 numbers and using 16 tasks
Time: 268 ms
Repetition 10: Report execution of HaMeR framework implementation, adding 262144 numbers and using 16 tasks
Time: 259 ms
Repetition 1: Report execution of HaMeR framework implementation, adding 262144 numbers and using 256 tasks
Time: 297 ms
Repetition 2: Report execution of HaMeR framework implementation, adding 262144 numbers and using 256 tasks
Time: 304 ms
Repetition 3: Report execution of HaMeR framework implementation, adding 262144 numbers and using 256 tasks
Time: 294 ms
Repetition 4: Report execution of HaMeR framework implementation, adding 262144 numbers and using 256 tasks
Time: 302 ms
Repetition 5: Report execution of HaMeR framework implementation, adding 262144 numbers and using 256 tasks
Time: 286 ms
Repetition 6: Report execution of HaMeR framework implementation, adding 262144 numbers and using 256 tasks
Time: 307 ms
Repetition 7: Report execution of HaMeR framework implementation, adding 262144 numbers and using 256 tasks
Time: 293 ms
Repetition 8: Report execution of HaMeR framework implementation, adding 262144 numbers and using 256 tasks
Time: 321 ms
Repetition 9: Report execution of HaMeR framework implementation, adding 262144 numbers and using 256 tasks
Time: 283 ms
Repetition 10: Report execution of HaMeR framework implementation, adding 262144 numbers and using 256 tasks
Time: 301 ms
Repetition 1: Report execution of HaMeR framework implementation, adding 1048576 numbers and using 1 tasks
Time: 1152 ms
Repetition 2: Report execution of HaMeR framework implementation, adding 1048576 numbers and using 1 tasks
Time: 1152 ms
Repetition 3: Report execution of HaMeR framework implementation, adding 1048576 numbers and using 1 tasks
Time: 1220 ms
Repetition 4: Report execution of HaMeR framework implementation, adding 1048576 numbers and using 1 tasks
Time: 1132 ms
Repetition 5: Report execution of HaMeR framework implementation, adding 1048576 numbers and using 1 tasks
Time: 1148 ms
Repetition 6: Report execution of HaMeR framework implementation, adding 1048576 numbers and using 1 tasks
Time: 1167 ms
Repetition 7: Report execution of HaMeR framework implementation, adding 1048576 numbers and using 1 tasks
Time: 1136 ms
Repetition 8: Report execution of HaMeR framework implementation, adding 1048576 numbers and using 1 tasks
Time: 1149 ms
Repetition 9: Report execution of HaMeR framework implementation, adding 1048576 numbers and using 1 tasks
Time: 1152 ms
Repetition 10: Report execution of HaMeR framework implementation, adding 1048576 numbers and using 1 tasks
Time: 1174 ms
Repetition 1: Report execution of HaMeR framework implementation, adding 1048576 numbers and using 16 tasks
Time: 852 ms
Repetition 2: Report execution of HaMeR framework implementation, adding 1048576 numbers and using 16 tasks
Time: 861 ms
Repetition 3: Report execution of HaMeR framework implementation, adding 1048576 numbers and using 16 tasks
Time: 927 ms
Repetition 4: Report execution of HaMeR framework implementation, adding 1048576 numbers and using 16 tasks
Time: 886 ms
Repetition 5: Report execution of HaMeR framework implementation, adding 1048576 numbers and using 16 tasks
Time: 854 ms
Repetition 6: Report execution of HaMeR framework implementation, adding 1048576 numbers and using 16 tasks
Time: 850 ms
Repetition 7: Report execution of HaMeR framework implementation, adding 1048576 numbers and using 16 tasks
Time: 858 ms
Repetition 8: Report execution of HaMeR framework implementation, adding 1048576 numbers and using 16 tasks
Time: 845 ms
Repetition 9: Report execution of HaMeR framework implementation, adding 1048576 numbers and using 16 tasks
Time: 858 ms
Repetition 10: Report execution of HaMeR framework implementation, adding 1048576 numbers and using 16 tasks
Time: 869 ms
Repetition 1: Report execution of HaMeR framework implementation, adding 1048576 numbers and using 256 tasks
Time: 977 ms
Repetition 2: Report execution of HaMeR framework implementation, adding 1048576 numbers and using 256 tasks
Time: 869 ms
Repetition 3: Report execution of HaMeR framework implementation, adding 1048576 numbers and using 256 tasks
Time: 946 ms
Repetition 4: Report execution of HaMeR framework implementation, adding 1048576 numbers and using 256 tasks
Time: 905 ms
Repetition 5: Report execution of HaMeR framework implementation, adding 1048576 numbers and using 256 tasks
Time: 958 ms
Repetition 6: Report execution of HaMeR framework implementation, adding 1048576 numbers and using 256 tasks
Time: 902 ms
Repetition 7: Report execution of HaMeR framework implementation, adding 1048576 numbers and using 256 tasks
Time: 985 ms
Repetition 8: Report execution of HaMeR framework implementation, adding 1048576 numbers and using 256 tasks
Time: 941 ms
Repetition 9: Report execution of HaMeR framework implementation, adding 1048576 numbers and using 256 tasks
Time: 892 ms
Repetition 10: Report execution of HaMeR framework implementation, adding 1048576 numbers and using 256 tasks
Time: 906 ms
Repetition 1: Report execution of Kotlin coroutines implementation, adding 262144 numbers and using 1 tasks
Time: 344 ms
Repetition 2: Report execution of Kotlin coroutines implementation, adding 262144 numbers and using 1 tasks
Time: 355 ms
Repetition 3: Report execution of Kotlin coroutines implementation, adding 262144 numbers and using 1 tasks
Time: 368 ms
Repetition 4: Report execution of Kotlin coroutines implementation, adding 262144 numbers and using 1 tasks
Time: 412 ms
Repetition 5: Report execution of Kotlin coroutines implementation, adding 262144 numbers and using 1 tasks
Time: 416 ms
Repetition 6: Report execution of Kotlin coroutines implementation, adding 262144 numbers and using 1 tasks
Time: 416 ms
Repetition 7: Report execution of Kotlin coroutines implementation, adding 262144 numbers and using 1 tasks
Time: 356 ms
Repetition 8: Report execution of Kotlin coroutines implementation, adding 262144 numbers and using 1 tasks
Time: 350 ms
Repetition 9: Report execution of Kotlin coroutines implementation, adding 262144 numbers and using 1 tasks
Time: 360 ms
Repetition 10: Report execution of Kotlin coroutines implementation, adding 262144 numbers and using 1 tasks
Time: 484 ms
Repetition 1: Report execution of Kotlin coroutines implementation, adding 262144 numbers and using 16 tasks
Time: 344 ms
Repetition 2: Report execution of Kotlin coroutines implementation, adding 262144 numbers and using 16 tasks
Time: 330 ms
Repetition 3: Report execution of Kotlin coroutines implementation, adding 262144 numbers and using 16 tasks
Time: 251 ms
Repetition 4: Report execution of Kotlin coroutines implementation, adding 262144 numbers and using 16 tasks
Time: 221 ms
Repetition 5: Report execution of Kotlin coroutines implementation, adding 262144 numbers and using 16 tasks
Time: 238 ms
Repetition 6: Report execution of Kotlin coroutines implementation, adding 262144 numbers and using 16 tasks
Time: 235 ms
Repetition 7: Report execution of Kotlin coroutines implementation, adding 262144 numbers and using 16 tasks
Time: 249 ms
Repetition 8: Report execution of Kotlin coroutines implementation, adding 262144 numbers and using 16 tasks
Time: 210 ms
Repetition 9: Report execution of Kotlin coroutines implementation, adding 262144 numbers and using 16 tasks
Time: 224 ms
Repetition 10: Report execution of Kotlin coroutines implementation, adding 262144 numbers and using 16 tasks
Time: 198 ms
Repetition 1: Report execution of Kotlin coroutines implementation, adding 262144 numbers and using 256 tasks
Time: 265 ms
Repetition 2: Report execution of Kotlin coroutines implementation, adding 262144 numbers and using 256 tasks
Time: 264 ms
Repetition 3: Report execution of Kotlin coroutines implementation, adding 262144 numbers and using 256 tasks
Time: 296 ms
Repetition 4: Report execution of Kotlin coroutines implementation, adding 262144 numbers and using 256 tasks
Time: 266 ms
Repetition 5: Report execution of Kotlin coroutines implementation, adding 262144 numbers and using 256 tasks
Time: 286 ms
Repetition 6: Report execution of Kotlin coroutines implementation, adding 262144 numbers and using 256 tasks
Time: 256 ms
Repetition 7: Report execution of Kotlin coroutines implementation, adding 262144 numbers and using 256 tasks
Time: 214 ms
Repetition 8: Report execution of Kotlin coroutines implementation, adding 262144 numbers and using 256 tasks
Time: 271 ms
Repetition 9: Report execution of Kotlin coroutines implementation, adding 262144 numbers and using 256 tasks
Time: 233 ms
Repetition 10: Report execution of Kotlin coroutines implementation, adding 262144 numbers and using 256 tasks
Time: 259 ms
Repetition 1: Report execution of Kotlin coroutines implementation, adding 1048576 numbers and using 1 tasks
Time: 1199 ms
Repetition 2: Report execution of Kotlin coroutines implementation, adding 1048576 numbers and using 1 tasks
Time: 1260 ms
Repetition 3: Report execution of Kotlin coroutines implementation, adding 1048576 numbers and using 1 tasks
Time: 1275 ms
Repetition 4: Report execution of Kotlin coroutines implementation, adding 1048576 numbers and using 1 tasks
Time: 1308 ms
Repetition 5: Report execution of Kotlin coroutines implementation, adding 1048576 numbers and using 1 tasks
Time: 1246 ms
Repetition 6: Report execution of Kotlin coroutines implementation, adding 1048576 numbers and using 1 tasks
Time: 1533 ms
Repetition 7: Report execution of Kotlin coroutines implementation, adding 1048576 numbers and using 1 tasks
Time: 1393 ms
Repetition 8: Report execution of Kotlin coroutines implementation, adding 1048576 numbers and using 1 tasks
Time: 1246 ms
Repetition 9: Report execution of Kotlin coroutines implementation, adding 1048576 numbers and using 1 tasks
Time: 1315 ms
Repetition 10: Report execution of Kotlin coroutines implementation, adding 1048576 numbers and using 1 tasks
Time: 1223 ms
Repetition 1: Report execution of Kotlin coroutines implementation, adding 1048576 numbers and using 16 tasks
Time: 794 ms
Repetition 2: Report execution of Kotlin coroutines implementation, adding 1048576 numbers and using 16 tasks
Time: 806 ms
Repetition 3: Report execution of Kotlin coroutines implementation, adding 1048576 numbers and using 16 tasks
Time: 822 ms
Repetition 4: Report execution of Kotlin coroutines implementation, adding 1048576 numbers and using 16 tasks
Time: 809 ms
Repetition 5: Report execution of Kotlin coroutines implementation, adding 1048576 numbers and using 16 tasks
Time: 781 ms
Repetition 6: Report execution of Kotlin coroutines implementation, adding 1048576 numbers and using 16 tasks
Time: 812 ms
Repetition 7: Report execution of Kotlin coroutines implementation, adding 1048576 numbers and using 16 tasks
Time: 814 ms
Repetition 8: Report execution of Kotlin coroutines implementation, adding 1048576 numbers and using 16 tasks
Time: 844 ms
Repetition 9: Report execution of Kotlin coroutines implementation, adding 1048576 numbers and using 16 tasks
Time: 808 ms
Repetition 10: Report execution of Kotlin coroutines implementation, adding 1048576 numbers and using 16 tasks
Time: 829 ms
Repetition 1: Report execution of Kotlin coroutines implementation, adding 1048576 numbers and using 256 tasks
Time: 809 ms
Repetition 2: Report execution of Kotlin coroutines implementation, adding 1048576 numbers and using 256 tasks
Time: 807 ms
Repetition 3: Report execution of Kotlin coroutines implementation, adding 1048576 numbers and using 256 tasks
Time: 871 ms
Repetition 4: Report execution of Kotlin coroutines implementation, adding 1048576 numbers and using 256 tasks
Time: 808 ms
Repetition 5: Report execution of Kotlin coroutines implementation, adding 1048576 numbers and using 256 tasks
Time: 815 ms
Repetition 6: Report execution of Kotlin coroutines implementation, adding 1048576 numbers and using 256 tasks
Time: 799 ms
Repetition 7: Report execution of Kotlin coroutines implementation, adding 1048576 numbers and using 256 tasks
Time: 826 ms
Repetition 8: Report execution of Kotlin coroutines implementation, adding 1048576 numbers and using 256 tasks
Time: 804 ms
Repetition 9: Report execution of Kotlin coroutines implementation, adding 1048576 numbers and using 256 tasks
Time: 810 ms
Repetition 10: Report execution of Kotlin coroutines implementation, adding 1048576 numbers and using 256 tasks
Time: 823 ms
Repetition 1: Report execution of Threads with barriers implementation, adding 262144 numbers and using 1 tasks
Time: 437 ms
Repetition 2: Report execution of Threads with barriers implementation, adding 262144 numbers and using 1 tasks
Time: 403 ms
Repetition 3: Report execution of Threads with barriers implementation, adding 262144 numbers and using 1 tasks
Time: 416 ms
Repetition 4: Report execution of Threads with barriers implementation, adding 262144 numbers and using 1 tasks
Time: 382 ms
Repetition 5: Report execution of Threads with barriers implementation, adding 262144 numbers and using 1 tasks
Time: 348 ms
Repetition 6: Report execution of Threads with barriers implementation, adding 262144 numbers and using 1 tasks
Time: 335 ms
Repetition 7: Report execution of Threads with barriers implementation, adding 262144 numbers and using 1 tasks
Time: 337 ms
Repetition 8: Report execution of Threads with barriers implementation, adding 262144 numbers and using 1 tasks
Time: 397 ms
Repetition 9: Report execution of Threads with barriers implementation, adding 262144 numbers and using 1 tasks
Time: 446 ms
Repetition 10: Report execution of Threads with barriers implementation, adding 262144 numbers and using 1 tasks
Time: 339 ms
Repetition 1: Report execution of Threads with barriers implementation, adding 262144 numbers and using 16 tasks
Time: 321 ms
Repetition 2: Report execution of Threads with barriers implementation, adding 262144 numbers and using 16 tasks
Time: 312 ms
Repetition 3: Report execution of Threads with barriers implementation, adding 262144 numbers and using 16 tasks
Time: 370 ms
Repetition 4: Report execution of Threads with barriers implementation, adding 262144 numbers and using 16 tasks
Time: 373 ms
Repetition 5: Report execution of Threads with barriers implementation, adding 262144 numbers and using 16 tasks
Time: 385 ms
Repetition 6: Report execution of Threads with barriers implementation, adding 262144 numbers and using 16 tasks
Time: 305 ms
Repetition 7: Report execution of Threads with barriers implementation, adding 262144 numbers and using 16 tasks
Time: 400 ms
Repetition 8: Report execution of Threads with barriers implementation, adding 262144 numbers and using 16 tasks
Time: 326 ms
Repetition 9: Report execution of Threads with barriers implementation, adding 262144 numbers and using 16 tasks
Time: 341 ms
Repetition 10: Report execution of Threads with barriers implementation, adding 262144 numbers and using 16 tasks
Time: 296 ms
Repetition 1: Report execution of Threads with barriers implementation, adding 262144 numbers and using 256 tasks
Time: 947 ms
Repetition 2: Report execution of Threads with barriers implementation, adding 262144 numbers and using 256 tasks
Time: 895 ms
Repetition 3: Report execution of Threads with barriers implementation, adding 262144 numbers and using 256 tasks
Time: 532 ms
Repetition 4: Report execution of Threads with barriers implementation, adding 262144 numbers and using 256 tasks
Time: 537 ms
Repetition 5: Report execution of Threads with barriers implementation, adding 262144 numbers and using 256 tasks
Time: 466 ms
Repetition 6: Report execution of Threads with barriers implementation, adding 262144 numbers and using 256 tasks
Time: 539 ms
Repetition 7: Report execution of Threads with barriers implementation, adding 262144 numbers and using 256 tasks
Time: 558 ms
Repetition 8: Report execution of Threads with barriers implementation, adding 262144 numbers and using 256 tasks
Time: 543 ms
Repetition 9: Report execution of Threads with barriers implementation, adding 262144 numbers and using 256 tasks
Time: 544 ms
Repetition 10: Report execution of Threads with barriers implementation, adding 262144 numbers and using 256 tasks
Time: 536 ms
Repetition 1: Report execution of Threads with barriers implementation, adding 1048576 numbers and using 1 tasks
Time: 1738 ms
Repetition 2: Report execution of Threads with barriers implementation, adding 1048576 numbers and using 1 tasks
Time: 1755 ms
Repetition 3: Report execution of Threads with barriers implementation, adding 1048576 numbers and using 1 tasks
Time: 1746 ms
Repetition 4: Report execution of Threads with barriers implementation, adding 1048576 numbers and using 1 tasks
Time: 1736 ms
Repetition 5: Report execution of Threads with barriers implementation, adding 1048576 numbers and using 1 tasks
Time: 1761 ms
Repetition 6: Report execution of Threads with barriers implementation, adding 1048576 numbers and using 1 tasks
Time: 1751 ms
Repetition 7: Report execution of Threads with barriers implementation, adding 1048576 numbers and using 1 tasks
Time: 1752 ms
Repetition 8: Report execution of Threads with barriers implementation, adding 1048576 numbers and using 1 tasks
Time: 1752 ms
Repetition 9: Report execution of Threads with barriers implementation, adding 1048576 numbers and using 1 tasks
Time: 1762 ms
Repetition 10: Report execution of Threads with barriers implementation, adding 1048576 numbers and using 1 tasks
Time: 1755 ms
Repetition 1: Report execution of Threads with barriers implementation, adding 1048576 numbers and using 16 tasks
Time: 1494 ms
Repetition 2: Report execution of Threads with barriers implementation, adding 1048576 numbers and using 16 tasks
Time: 1492 ms
Repetition 3: Report execution of Threads with barriers implementation, adding 1048576 numbers and using 16 tasks
Time: 1494 ms
Repetition 4: Report execution of Threads with barriers implementation, adding 1048576 numbers and using 16 tasks
Time: 1480 ms
Repetition 5: Report execution of Threads with barriers implementation, adding 1048576 numbers and using 16 tasks
Time: 1492 ms
Repetition 6: Report execution of Threads with barriers implementation, adding 1048576 numbers and using 16 tasks
Time: 1729 ms
Repetition 7: Report execution of Threads with barriers implementation, adding 1048576 numbers and using 16 tasks
Time: 1805 ms
Repetition 8: Report execution of Threads with barriers implementation, adding 1048576 numbers and using 16 tasks
Time: 1726 ms
Repetition 9: Report execution of Threads with barriers implementation, adding 1048576 numbers and using 16 tasks
Time: 1486 ms
Repetition 10: Report execution of Threads with barriers implementation, adding 1048576 numbers and using 16 tasks
Time: 1500 ms
Repetition 1: Report execution of Threads with barriers implementation, adding 1048576 numbers and using 256 tasks
Time: 1803 ms
Repetition 2: Report execution of Threads with barriers implementation, adding 1048576 numbers and using 256 tasks
Time: 1880 ms
Repetition 3: Report execution of Threads with barriers implementation, adding 1048576 numbers and using 256 tasks
Time: 1720 ms
Repetition 4: Report execution of Threads with barriers implementation, adding 1048576 numbers and using 256 tasks
Time: 1779 ms
Repetition 5: Report execution of Threads with barriers implementation, adding 1048576 numbers and using 256 tasks
Time: 1717 ms
Repetition 6: Report execution of Threads with barriers implementation, adding 1048576 numbers and using 256 tasks
Time: 1780 ms
Repetition 7: Report execution of Threads with barriers implementation, adding 1048576 numbers and using 256 tasks
Time: 1712 ms
Repetition 8: Report execution of Threads with barriers implementation, adding 1048576 numbers and using 256 tasks
Time: 1717 ms
Repetition 9: Report execution of Threads with barriers implementation, adding 1048576 numbers and using 256 tasks
Time: 1888 ms
Repetition 10: Report execution of Threads with barriers implementation, adding 1048576 numbers and using 256 tasks
Time: 1744 ms
====================================================================================================
