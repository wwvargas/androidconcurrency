package com.example.concurrencyeval.util

class TestReport (val problemId: Int, val runReport: RunReport, val testParams: Map<String, Int>)